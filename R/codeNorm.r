
option_list = list(
  make_option(c("-f", "--file"), type="character", default=NULL, 
              help="Raw count matrix file.", metavar="character"),
  make_option(c("-d", "--design"), type="character", default=NULL, 
              help="Simple design file, with samples in the first row, and group in the second row.", metavar="character"),
  make_option(c("-s", "--separator"), type="character", default=",", 
              help="Field separator for both -f and -d", metavar="character"),
  make_option(c("-o", "--out"), type="character", default=NULL, 
              help="output file name", metavar="character"),
  make_option(c("-n", "--num_iteration"), type="numeric", default=0, 
              help="Number of iteration for improved normalisation method based on first removal of DE genes.", metavar="numeric"),
  make_option(c("-v", "--all_vs_all_dge"), type="logical", default=FALSE, 
              help="Compute all vs all differentiall gene expression. parameters are: pvalue = 0.01 ; log2fc = 2", metavar="boolean")
); 

opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);


if (is.null(opt$file)){
  print_help(opt_parser)
  stop("The --file argument is required.", call.=FALSE)
}
if (is.null(opt$design)){
  print_help(opt_parser)
  stop("The --design argument is required.", call.=FALSE)
}

if (is.null(opt$out)){
  build_output_file <- unlist(strsplit(opt$file, "\\.", fixed = FALSE)) ###We split this file on dot.
  if(length(build_output_file) > 1){
    opt$out <- paste0(build_output_file[-length(build_output_file)], "_normalized.csv", collapse = "")
  } else {
    opt$out <- paste0(build_output_file, "_normalized.csv", collapse = "")
  }
  print(paste0("No output file name is provided. Output file has been set to : ", opt$out, collapse = ""))
}

if(is.null(opt$num_iteration)){
  print("Normalisation after DE removal will not be performed.")
  opt$num_iteration = 0
}

if(opt$num_iteration < 0 || opt$num_iteration > 5 || is.numeric(opt$num_iteration) == FALSE){
  print_help(opt_parser)
  stop("The --num_iteration argument must be a integer between 0 and 5.", call.=FALSE)
}

cat("\n")
print(paste("The field separator is set to :", opt$separator, sep = " "))
cat("\n")

if(file.exists(opt$out)){
  print_help(opt_parser)
  stop(paste0(opt$out, " already exists. Please choose another file. You can use the --out option for this."))
}

###Read and check count data.
if(file.exists(opt$file)){
  my_count <- as.matrix(read.delim(file = opt$file, header = TRUE, sep = opt$separator, row.names = 1))
  cat("Count matrix preview : \n")
  print(head(my_count))
  if(ncol(my_count) < 2){
    stop("There is less than two columns in your count table. Did you correctly set the separator ?")
  }
} else {
  stop(paste0(opt$file, " not found."))
}

cat("\n")

###Read and check design file.
if(file.exists(opt$design)){
  my_design <- read.delim(file = opt$design, header = TRUE, sep = opt$separator, row.names = 1)
  cat("Design matrix preview : \n")
  print(head(my_design))
  if(ncol(my_design) != 1){
    stop("There is too much columns in the design file. This field must contain only one column for sample names, and one column for the condition name associated with each sample. So, two column.")
  }
} else {
  stop(paste0(opt$design, " not found."))
}

cat("\n")

###Read and check that design and count are compatible.
if(nrow(my_design) == ncol(my_count)){
  cat("There is the same number of samples in the count and design file. Good\n")
} else {
  print(rownames(my_design))
  print(colnames(my_count))
  stop("There is a different number of samples in the count and design matrix. Please check the above list.")
}

if(all(colnames(my_count) %in% rownames(my_design))){
  cat("All count sample are in the design matrix. Good\n")
} else {
  print("Bellow you'll find the common column between your two input files.")
  print(colnames(my_count[! colnames(my_count) %in% rownames(my_design)]))
  print(colnames(my_count))
  print(rownames(my_design))
  stop("There is some differences between samples in the count and design matrix.")
}

if(all(colnames(my_count) == rownames(my_design))){
  cat("All count sample are in the same order. Good\n")
} else {
  print(colnames(count[colnames(my_count) != rownames(my_design)]))
  stop("There is some differences between samples between in count and design matrix. Please check the above list and check in the files.")
}

require("TCC", quietly = FALSE, include.only = c("calcNormFactors", "getNormalizedData"))
TCC <- new("TCC", my_count, my_design)
TCC <- TCC::calcNormFactors(TCC, norm.method = "tmm", test.method = "edger", iteration = opt$num_iteration, FDR = 0.1, floorPDEG = 0.05)
norm_count <- TCC::getNormalizedData(TCC)

###Creating the design matrix



cat("\nProcessing with normalization...\n")
my_normalized_count <- round(x = normalisation(my_count, my_design, opt$num_iteration), digits = 2)

cat("\nNormalized count preview : \n")
print(head(my_normalized_count))

write.table(data.frame("Gene"=rownames(my_normalized_count),my_normalized_count), file = opt$out, row.names=FALSE, sep = opt$separator)

if(opt$all_vs_all_dge){
  cat("\nComputing differentially expressed genes.\n")
  DGE_list <- all_vs_all_edger(TCC = TCC, my_design)
  if(!dir.exists("differential_expression_results")){
    dir.create("differential_expression_results")
  }
  for(i in names(DGE_list)){
    if(!file.exists(paste0("./differential_expression_results/", i, ".csv"))){
      write.table(x = DGE_list[[i]], file = paste0("./differential_expression_results/", i, ".csv"), sep = opt$separator, row.names = FALSE, col.names = TRUE)
    } else {
      stop(paste0("The DGE list file : ", "./differential_expression_results/", i, ".csv", " already exists. Please make sure that the analyse has not been done yet, or just rename the differential_expression_results folder into something else."))
    }
  }
}




cat("\nDone.\n")
