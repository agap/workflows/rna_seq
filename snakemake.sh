#!/bin/bash 
#
#SBATCH -J smk-rnaseq
#SBATCH -o logs/smk-rnaseq."%j".out
#SBATCH -e logs/smk-rnaseq."%j".err 
##SBATCH --mail-type=END
##SBATCH --mail-user=YourEmailHere
#SBATCH -p agap_long

module purge
module load snakemake/7.15.1-conda


## Using conda
snakemake --profile slurm --rerun-incomplete --jobs 30 --cores 100 -p --use-conda 

## Or using modules installed on the cluster
#snakemake --profile slurm --rerun-incomplete --jobs 30 --cores 100 -p --use-envmodules  
