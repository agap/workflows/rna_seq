---
tags: [rna-seq, snakemake, workflow]
---

# RNA-seq workflow <!-- omit from toc -->

- [1. Objectives](#1-objectives)
- [2. Steps](#2-steps)
  - [2.1. Trimming and quality control](#21-trimming-and-quality-control)
  - [2.2. Mapping / pseudo mapping](#22-mapping--pseudo-mapping)
  - [2.3. Count](#23-count)
- [3. Dependency](#3-dependency)
  - [A note about conda](#a-note-about-conda)
- [4. Tutorial](#4-tutorial)
  - [4.1. Importing the workflow](#41-importing-the-workflow)
  - [4.2. Configuring the workflow](#42-configuring-the-workflow)
    - [4.2.1. General parameters](#421-general-parameters)
    - [4.2.2. Tool specific Parameters](#422-tool-specific-parameters)
    - [4.2.3. Envmodule configuration](#423-envmodule-configuration)
    - [4.2.4. Cluster parameter](#424-cluster-parameter)
  - [4.3. Running the workflow](#43-running-the-workflow)
    - [4.3.1. Base mode](#431-base-mode)
    - [4.3.2. Raw data information mode](#432-raw-data-information-mode)
- [5. Output file description](#5-output-file-description)
  - [5.1. Normal mode](#51-normal-mode)
  - [5.2. Raw data information mode](#52-raw-data-information-mode)

## 1. Objectives

This workflow allow to get a count matrix from raw fastq files, by using a reference annotated genome. It is possible to choose between two aligner (hisat2 and star) and also between two pseudo mapping tools (salmon and kallisto). The analysis can be run using multiple tools at the same time in order to compare their output.

The input and output format should be compatible with the DIANE tool, which allows to perform transcriptomics analysis using the count matrix produced by the pipeline.

:warning: : this workflow is still under development. If you have any question or suggestion, please contact [Alexandre Soriano](mailto:Alexandre.soriano@cirad.fr)

## 2. Steps

![Workflow](./workflow_img.svg)

There is many intermediate steps, but the workflow can be separated into 3 main parts.

### 2.1. Trimming and quality control

**Tools** : fastqc / cutadapt

The quality control part take the raw input reads and remove low quality bases as well as adapter sequences. The output is cleaned fastq files which can be used for the following analysis. 

### 2.2. Mapping / pseudo mapping

**Tools** : hisat2 / star / kallisto / salmon

Align reads either to a reference genome (hisat2 and star) or to a reference transcriptome (kallisto/salmon/RSEM)

### 2.3. Count

**Tools** : Stringtie / featureCount

Quantify reads in genes using hisat2 or/and star mapping

## 3. Dependency

There is two ways to launch the workflow. One using the `--use-envmodules` parameter, which require all the tools to be installed on the cluster and properly indicated in the configuration file (see [configuring the workflow](#142-configuring-the-workflow) part). Otherwise you can also install all of them automaticaly using conda, using the `--use-conda` parameter for snakemake. By doing this, all the tools will be downloaded and setup automatically. You should first try the second option, if you're not sure about the one to chose.

- **cutadapt** : Cutadapt finds and removes adapter sequences, primers, poly-A tails and other types of unwanted sequence from your high-throughput sequencing reads. (<https://cutadapt.readthedocs.io/en/stable/>)
- **hisat2** : HISAT2 is a fast and sensitive alignment program for mapping next-generation sequencing reads (both DNA and RNA) to a population of human genomes as well as to a single reference genome (<http://daehwankimlab.github.io/hisat2/>)
- **samtools** : Reading/writing/editing/indexing/viewing SAM/BAM/CRAM format (<http://www.htslib.org/>)
- **picard** : Picard is a set of command line tools for manipulating high-throughput sequencing (HTS) data and formats such as SAM/BAM/CRAM and VCF (<https://broadinstitute.github.io/picard/>)
- **stringtie** : StringTie is a fast and highly efficient assembler of RNA-Seq alignments into potential transcripts (<https://ccb.jhu.edu/software/stringtie/>)
- **deepTools** : deepTools is a suite of python tools particularly developed for the efficient analysis of high-throughput sequencing data, such as ChIP-seq, RNA-seq or MNase-seq. (<https://deeptools.readthedocs.io/en/develop/>)
- **prepDE** : a tool alowing to extract count matrix from stringtie output (<https://ccb.jhu.edu/software/stringtie/>)
- **gffread** : GFF/GTF utility providing format conversions, filtering, FASTA sequence extraction and more. (<https://github.com/gpertea/gffread>)
- **subread** featureCount :  a software program developed for counting reads to genomic features such as genes, exons, promoters and genomic bins.(<https://subread.sourceforge.net/>)
- **fastqc** : A quality control tool for high throughput sequence data. (<https://github.com/s-andrews/FastQC>)
- **star** : Spliced Transcripts Alignment to a Reference (<https://github.com/alexdobin/STAR>)
- **multiqc** : Aggregate results from bioinformatics analyses across many samples into a single report (<https://multiqc.info/>)
- **kallisto** : kallisto is a program for quantifying abundances of transcripts from bulk and single-cell RNA-Seq data, or more generally of target sequences using high-throughput sequencing reads. (<https://pachterlab.github.io/kallisto/about>)
- **salmon** : Salmon is a tool for wicked-fast (they wrote it like that) transcript quantification from RNA-seq data. (<https://salmon.readthedocs.io/en/latest/index.html>)
- **RSEM** : RSEM is a software package for estimating gene and isoform expression levels from RNA-Seq data. (<https://github.com/deweylab/RSEM>)
- **java**
- **python3**
- **R 4**
  - GenomicFeatures
  - edgeR
  - tcc
  - optparse
  - stringr
  - ggplot2
  - ggrepel
  - ade4
  - tximport
  - rhdf5

### A note about conda

If you plan to use the `--use-conda` option on the muse cluster, you should also update conda configuration as follow :

```bash
## Make a backup of you .condarc file, which will be modified.
cp ~/.condarc ~/.condarc_backup_rnaseqsnakemake

## Unload all loaded modules
module purge

## Load snakemake
module load snakemake/7.15.1-conda

## Add a directory outside of you home directory for storing packages downloaded by conda. replace "YOURUSERNAME" by your real username on the cluster.
conda config --add pkgs_dirs /lustre/YOURUSERNAME/.conda_pkgs_dirs
```

This will update your .condarc file and you should now be set.

**Note**  : this command is for the muse cluster, you should adapt it if you're working somewhere else.

## 4. Tutorial

This tutorial will guide you through the configuration of the workflow. Here we consider that you are connected to a cluster, and located in the directory in which you want to run the analysis.

For exemple, if you use the "muse" cluster, you can do something like this, after connecting.

```bash
cd /lustre/YourAccountName
mkdir rna_seq
cd rna_seq
```

### 4.1. Importing the workflow

First you need to clone the repository. The following command will create directory named "my_rnaseq_analysis" containing all the needed files in the current directory. You can change "my_rnaseq_analysis" by whatever name you want.

```console
git clone https://gitlab.cirad.fr/agap/workflows/rna_seq.git ./my_rnaseq_analysis
```

Then go to this directory

```console
cd my_rnaseq_analysis
```

Then you will need to configure the workflow.

### 4.2. Configuring the workflow

All the configuration is done in the `config.yaml` file. This file is divided in two parts : the general parameters and the tool-specific parameters

#### 4.2.1. General parameters

These are some parameters related to the workflow himself.

- **trimming** : (true/false) enable or disable trimming.
- **outputBigWig** : (true/false) output bigwig files. This files can be loaded into a genome browser to display read coverage per base.
- **checkBam** : (true/false) output some stats about mapping using the samtools flagstat command.
- **mark_duplicates** : If True, mark (but not remove) PCR and optical duplicates. if False, this step is skipped.
- **read_length** : (int) length of reads, before any kind of trimming.
- **check_input_file_name** : (true/false) check if your input file name is compliant with what DIANE expect for input.
- **tmpDir** : (path) Path to a custom directory where temporary files will be stored. Can be set to /tmp or any other dir.

These are some parameters related to input file.

- **genome**: (path) Your genome in fasta format.
- **transcriptome**: (path) Your transcriptome in fasta format.
- **annotation_gff3** : (path) annotation file in gff3 format.
- **annotation_gtf** : (path) Annotation file in gtf format.

:warning: : you must specify at least a genome **OR** a transcriptome (or both). You can use a transcriptome without a genome, and a genome without a transcriptome. Leave the fields that you don't need empty (""). If you specify a genome, you must specify an annotation in gff3 and gtf format. If you don't use a genome, it's not necessary.

If you don't have your annotation in gtf format, you can convert a gff3 file to a gtf file using the `gffread` tool, using the following command :

```bash
module load gffread

gffread -E mon_fichier.gff3 -T -o mon_fichier.gtf
```

**Note** : this step is not automated because some error may arise when converting gff to gtf. These errors can be interesting to look at, as they may be cause by malformed gff files.

**sample_list** : (path) your input file list. Must be a 3 column tab separated file. The first column must contain the following header : Name read_R1 read_R2. Each following line must define a sample, with a unique name and the path to the forward and reverse corresponding reads.

| Name  | read_R1                   | read_R2                   |
|-------|---------------------------|---------------------------|
| condition.one_1 | path/to/one_1_R1.fastq.gz | path/to/one_1_R2.fastq.gz |
| condition.one_2 | path/to/one_2_R1.fastq.gz | path/to/one_2_R2.fastq.gz |
| condition.two_1 | path/to/two_1_R1.fastq.gz | path/to/two_1_R2.fastq.gz |
| condition.two_2 | path/to/two_2_R1.fastq.gz | path/to/two_2_R2.fastq.gz |

To get a first draft of file to work with, you can go into the directory where your fastq are stored, and launch the following command :

```bash
echo -e "Name\tread_R1\tRead_R2"; for i in `ls *R1*`; do name=${i%%_R*}; R2=${i/R1/R2}; echo -e "$name\t$(pwd)/$i\t$(pwd)/$R2"; done
```

**transcript_to_gene_mapping** : (path) a file linking each transcript to the gene he come from. This may be useful for some tools (stringtie, salmon, kallisto, RSEM) that count the number of reads falling in each transcript and can summarize this information to the gene level. This file must contain two columns, separated by tabulation. The first column contains the gene ID, and the second column a transcript associated with this gene. In the following example, two genes are associated with two transcripts (gene 1 and 3) and one gene is associated with only one transcript (gene 2)

| gene   | transcript |
|--------|------------|
| gene_1 | gene_1.1   |
| gene_1 | gene_1.2   |
| gene_2 | gene_2.1   |
| gene_3 | gene_3.1   |
| gene_3 | gene_3.2   |

**output_main_results_dir** : you can specify a directory where main workflow output will be copied after a successful run. This is implemented to avoid unattended loss of data.

#### 4.2.2. Tool specific Parameters

```yaml
fastqc:
  threads: 2

#Cutadapt is used for the read trimming step. The tool is set on paired-end mode.
Cutadapt:
  Quality_value: "20,20"                  #Trimming on quality for the 3' and 5' end. The two values must be separated by a ",". This will remove the bad quality parts of the reads. 20,20 is a good compromise. 30,30 will be more  stringent, but will result in a bigger loss of data.
  min_length: "35"                        #Reads with a length above this value will be deleted
  forward_adapter: "AGATCGGAAGAGCACACGTCTGAACTCCAGTCA"     #Forward adapter to look for
  reverse_adapter: "AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT"     #Reverse adapter to look for
  threads: 8                             #Number of threads used for cutadapt
  max_n: "0"  ## Max number of n in one read.
  options: "--pair-filter=any --nextseq-trim=20"                             #This parameter can be used to add any parameter that you wan't to use. --nextseq-trim=20 should be used only on novaseq sequencers
                                          

#Hisat2 is used for the mapping step.
hisat2:
  threads: 12                                      #Number of thread used for the mapping step. This will slightly reduce the running time.
  options: "--sensitive --rna-strandness RF --dta --max-intronlen 10000"                            #Any parameter that you would like to add to the raw_mapping step.
  used: True

#Star is used for the mapping step.
star:
  threads: 8
  options: ""
  used: True

kallisto:
  threads: 2
  options: "--bias --seed 42 -bootstrap-samples=100"
  strandness: "--rf-stranded "
  used: True

salmon:
  threads: 4
  options: "--seqBias --gcBias "
  used: True  

rsem:
  threads: 4
  options: "--strandedness reverse --seed 100"
  used: true

samtools: 
  threads: 2 ###Number of threads used for sorting and indexing files.

stringtie:
    options: "--rf -M 1"

bamCoverage:
  threads: 4

featureCount:
  threads: 12
  quality_cutoff: 0
  strand: 0
  options: "-C -p -t exon -g gene_id -M --fraction"

multiqc:
  used: False
```

#### 4.2.3. Envmodule configuration

If the workflow is launched using the `--use-envmodules`parameter, all the env-modules must be indicated here.

```yaml
modules:
  cutadapt: "cutadapt/3.5-bin"
  hisat2: "hisat2/2.2.1"
  samtools: "samtools/1.14-bin"
  picard: "picard-tools/2.24.0"
  stringtie: "stringtie/2.1.6"
  deepTools: "deepTools/3.5.1"
  bedops: "bedops/2.4.39"
  rseqc: "RSeQC/5.0.1"
  prepDE: "stringtie/2.1.6"
  gffread: "gffread/0.12.6"
  python3: "python/3.8.2" 
  subread: "subread/2.0.1"
  fastqc: "fastqc/0.11.7"
  star: "star/2.7.3a"
  multiqc: "multiqc/1.9" 
  kallisto: "kallisto/0.46.1"
  salmon: "salmon/1.5.2"
  java: "java/jdk1.8.0_271"
  R: "R/packages/4.1.0"
  rsem: "rsem/1.3.3-bin"
```

#### 4.2.4. Cluster parameter

The workflow is configured to run using the "muse" cluster. If you are using another cluster, you should modify the `config_cluster_params.yaml` file in order to setup the queue and time limit correctly, according to the cluster you are using.

```yaml
queue:
  short: agap_short
  normal: agap_normal
  long: agap_long  
  bigmem: agap_bigmem  

time_limit:
  instantaneous: 1
  very_short: 5
  short: 60 
  normal: 2880  
  long: 10080
  bigmem: 10080
```

for example, if you use the **IFB** cluster, the file should look like this. Note that "long" is used multiple time, because there is no "normal" queue on thie particular cluster.

```yaml
queue:
  short: short
  normal: long
  long: long  
  bigmem: bigmem  

time_limit:
  instantaneous: 1
  very_short: 5
  short: 1440
  normal: 10080  
  long: 10080
  bigmem: 10080
```

**Note** : the time limit that have the same name as a queue (short, for exemple) should be set to the time limit of the corresponding queue. You can ask your sysadmin for this information. The other parameters are for specific jobs with either very short or long running time.

### 4.3. Running the workflow

#### 4.3.1. Base mode

The first thing you must do is creating a directory called "logs" in the same directory as the Snakefile.

```console
mkdir -p logs
```

:warning: : You must create this directory. Otherwise, nothing will work properly.

After everything has been setup, you can finaly launch the workflow. You first should check that everything looks fine by running "dry run" :

```console
module load snakemake/7.15.1-conda
snakemake -n
```

If no error message shows up, you can run the workflow for good. You should use a small script to do so.

```bash
#!/bin/bash 
#
#SBATCH -J smk-rnaseq
#SBATCH -o smk-rnaseq."%j".out
#SBATCH -e smk-rnaseq."%j".err 
#SBATCH -p agap_long

module purge
module load snakemake/7.15.1-conda

mkdir -p logs/
snakemake --profile slurm --jobs 30 --cores 100 -p --use-conda
```

if this script is called `snakemake.sh`, you can finaly type :

```console
sbatch snakemake.sh
```

And the workflow should start. This can take a few days.

#### 4.3.2. Raw data information mode

You can also run the workflow in a special mode that will only output informations about raw data. for doing so, you can use a stighyly modified version of the script presented in the previous part

```bash
#!/bin/bash 
#
#SBATCH -J smk-rnaseq
#SBATCH -o smk-rnaseq."%j".out
#SBATCH -e smk-rnaseq."%j".err 
#SBATCH -p agap_long

module purge
module load snakemake/7.15.1-conda

mkdir -p logs/
snakemake --profile slurm --jobs 30 --cores 100 -p --use-conda quality_controll_and_diagnostic
```

And run the script :

```console
sbatch snakemake.sh
```

## 5. Output file description

### 5.1. Normal mode

Primary output are stored in `04_results` directory. These are the different count matrix from stringtie and featureCount depending if you enabled any mapping tools, the output directory from salmon and kallisto if they were enabled, and the output from RSEM. You can also find the output of multiqc, which shows informations about the different tools that were run.

You can also find a directory called `05_analysis` Here you can find some output based on the tools you enabled. Basicaly, these are normalized count matrix (using TMM), Principal component analysis, and basic differential expression results.

To perform some quality control, you should check multiqc output and the PCA

If you use the [Raw data information mode](#raw-data-information-mode), you will also find a folder named  \"00_diagnostic\" in which you will find informations about raw read quality score, intron length distribution in the input gff/gtf file, and informations about read orientation. These informations may help you with setting up you parameters.

### 5.2. Raw data information mode

Output for the [Raw data information mode](#raw-data-information-mode)  will be stored in a dir named `00_diagnostic\`. in which you will find informations about raw read quality score, intron length distribution in the input gff/gtf file, and informations about read orientation. These informations may help you with setting up you parameters. fastqc report can also be found in `raw_data/fastqc/`
