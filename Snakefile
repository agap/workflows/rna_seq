configfile: "config.yaml"
configfile: "config_cluster_params.yaml"

from snakemake.utils import min_version
import os,re
from datetime import datetime
import csv
import math
from datetime import datetime
import yaml
import shutil
# import ipdb
#import re
#from os.path import join
#import glob
#

##TODO : Add a new filter for stringtie output to format it for DIANE. DONE   
##TODO : faire une doc, sombre naze. DONE
##TODO : Clean les io
##TODO : mieux gérer multiqc (plusieurs rapports) DONE
##TODO Renommer le 03 en quelque chose de générique : DONE
##TODO Sortir quelque chose de salmon et kallisto. une PCA, et une DGE au moins. : DONE
##TODO : Le faire joliement... 
##TODO : Auto-detection of strandness ### 1/2 Done



#This workflow has been designed in order to analyse paired-end RNA-seq data and produce biggwig files that can be implemented in a genome browser. It also produce files with stringtie from wich you can extract counting informations for Diff expr.

min_version("7.00.0")

###Get current time. For logs, especially picard.
time=datetime.now()
current_time = time.strftime("%d-%m-%y.%Hh%M")

# if not os.path.exists("logs/"+current_time):
#     os.makedirs("logs/"+current_time)


#########################
###Import Fastq file list
csv_filename = config["sample_list"]

fastq_dict = {}

with open(csv_filename) as f:
    reader = csv.DictReader(f, delimiter="\t")
    for row in reader:
        fastq_dict[row["Name"]] = {}
        fastq_dict[row["Name"]]["R1"] = row["read_R1"] 
        fastq_dict[row["Name"]]["R2"] = row["read_R2"]

sample_keys = fastq_dict.keys()

###Check if there is both a genome and a transcriptome.
if(config["transcriptome"] == "" and config["genome"] == ""):
    raise Exception("ERROR :you must specify either a genome OR a transcriptome.")

###Force some parameters to False if they are not relevant (if only a transcriptome is specify, for exemple, we disable all mapping related tools)
if(config["transcriptome"] != "" and config["genome"] == ""):
    config["mark_duplicates"] = False
    config["outputBigWig"] = False
    config["checkBam"] = False
    config["hisat2"]["used"] = False
    config["star"]["used"] = False

### Choose between user inputed transcriptome or extracted one, if the field is empty.
def choose_transcriptome_input():
    if(config["transcriptome"] != ""):
        return config["transcriptome"]
    else:
        return expand("raw_data/genome/{genome}_tx.fasta", genome=genome_name)
        
#Check that sample name corresponds to what DIANE wants. We will also disable R stuff if the pattern is not good.
if(config["check_input_file_name"] == True):
    print("Checking identifier according to DIANE expected input...")
    all_valid = True
    for key in sample_keys:   # iter on both keys and values
        x = re.search("^[^_]*_\d+$", key)
        if(x):
            print(key + " is a valid name")
        else:
            print("\033[1;31mWarning : " + key + " is not a valid name...\033[0m")
            all_valid = False

    condition_dict = {}
    ##Verify if asked. Crash if all samples are not good.
    if(config["check_input_file_name"] == True and all_valid == False):
        if (not all_valid):
            raise Exception("ERROR : There is invalid sample name in your config file. You can either modify the faulty names or disable the check_input_file_name parameter in the config.yaml file.")
    else:
        print("\033[1;32mAll ID are correct.\033[0m")


    print("In depth sample/condition check...")
    for key in sample_keys:   # iter on both keys and values
        x = re.search("^([^_]*)_(\d+)$", key)
        if x.groups()[0] not in condition_dict:
            condition_dict[x.groups()[0]] = 0
        condition_dict[x.groups()[0]] = condition_dict[x.groups()[0]] + 1

    ###Check that all conditions have more than one replicate and also print them. Here we just create the data structure.
    if(config["check_input_file_name"] == True):

        ##There is only one condition.
        if(len(condition_dict) == 1):
            raise Exception("There is only one condition in your dataset.  You can disable the check_input_file_name parameter in the condig.yaml file if you still wants to run the pipeline")

        print("Detected conditions and number of replicates : ")
        for key in condition_dict:
            print(key + " : " + str(condition_dict[key]))
            ## There is only one replicate in a condition.
            if (condition_dict[key] == 1):
                raise Exception("Condition \"" + key + "\" has only one replicate. You shoud not include this in your analysis. You can disable the check_input_file_name in the condig.yaml file if you still wants to run the pipeline")
            
        print("\033[01;32mAll your conditions look good !\033[0m")



# ipdb.set_trace()

###Can be given to increase mem on each crash
def get_mem_mb(wildcards, attempt):
    if(attempt == 1):
        return 4000
    elif(attempt == 2):
        return 6000
    else:
        return 10000

#resources: mem_mb=lambda wildcards, input, attempt: (input.size//1000000) * attempt * 10

def get_mem_picard_sample(wildcards, threads, input):
    return 1 * min(max(1.1 * input.size_mb, 8192), 20480)

def get_mem_hisat2_index(wildcards, threads, input):
    return  max((input.size_mb * 75), 8192)

###For the way this tool use ram
def get_mem_fastqc(wildcards, threads):
    return threads * 1024

def get_mem_picard(wildcards, threads):
    return threads * 8192

def get_mem_star_mapping(wildcards, threads):
    return threads * 4096

def get_mem_hisat_mapping(wildcards, threads):
    return max(threads*1024, 4096) + 4096

def get_mem_featureCount(wildcards, threads):
    return min(threads*1024, 16384) + 4096

def get_mem_cutadapt(wildcards, threads):
    return min(threads*1024, 4096)


###Return number of base in fasta. Used to count genome size.
def get_genome_size(wildcards):
    header = None
    length = 0
    with open(wildcards) as fasta:
        for line in fasta:
            line = line.rstrip()
            if not line.startswith('>'):
                length += len(line)
    if length:
        return(length)



# mapping_tool = glob_wildcards("hisat2", "star")

###Set mapping tools
selected_mapping_tools = []
if config["genome"] != "": ##If there is no genome, just disable mapping based tool.
    if (config["hisat2"]["used"] == True):
        selected_mapping_tools.append("hisat2")
    if (config["star"]["used"] == True):
        selected_mapping_tools.append("star")

###Set pseudo_mapping_tools
selected_pseudo_mapping_tools = []
if (config["kallisto"]["used"] == True):
    selected_pseudo_mapping_tools.append("kallisto")
if (config["salmon"]["used"] == True):
    selected_pseudo_mapping_tools.append("salmon")

selected_rsem = False
if (config["rsem"]["used"] == True):
    selected_rsem = True

#If no mapping or pseudo mapping tool is specified, just leave.
if(selected_mapping_tools == [] and selected_pseudo_mapping_tools == [] and selected_rsem == False):
    raise Exception("You must select at least one mapping (star / hisat2) or pseudo_mapping (salmon / kallisto) tool, or the rsem tool. Please check your config.yaml file.")


selected_quantification_tool = []
if (config["featureCount"]["used"] == True):
    selected_quantification_tool.append("featureCount")
if (config["stringtie"]["used"] == True):
    selected_quantification_tool.append("stringtie")

if(selected_quantification_tool == []):
    raise Exception("You must select at least one quantification tool (stringtie or featureCount). Please check your config.yaml file.")

genome_size = 1
if (config["genome"] != "" and config["star"]["used"] == True):
    genome_size = get_genome_size(config["genome"])

    

read_length, ={config["read_length"]}
my_temp_dir, ={config["tmpDir"]}

#Here we are collecting the ids of the different sample. the pattern is : xxx_R1_001.fastq.gz. Files MUST be named this way. it will be usefull for the variant calling step, when all of these file will be merged to one file. Without this, snakemake won't be able to do it.
# sample_ids, = glob_wildcards("data/Raw_reads/{sample}_R1.fastq.gz")

genome_name = ""
if config["genome"] != "":
    genome_prefix = re.sub('\.fasta$|\.fa$', '', config["genome"])
    genome_name = os.path.basename(genome_prefix)

transcriptome_name = ""
if config["transcriptome"] != "":
    transcriptome_name = os.path.basename(re.sub('\.fasta$|\.fa$', '', config["transcriptome"]))

onstart:
    print("""

   _____             _                        _          _____  _   _                          __          __        _     __ _               
  / ____|           | |                      | |        |  __ \| \ | |   /\                    \ \        / /       | |   / _| |              
 | (___  _ __   __ _| | _____ _ __ ___   __ _| | _____  | |__) |  \| |  /  \   ___  ___  __ _   \ \  /\  / /__  _ __| | _| |_| | _____      __
  \___ \| '_ \ / _` | |/ / _ \ '_ ` _ \ / _` | |/ / _ \ |  _  /| . ` | / /\ \ / __|/ _ \/ _` |   \ \/  \/ / _ \| '__| |/ /  _| |/ _ \ \ /\ / /
  ____) | | | | (_| |   <  __/ | | | | | (_| |   <  __/ | | \ \| |\  |/ ____ \\\\__ \  __/ (_| |    \  /\  / (_) | |  |   <| | | | (_) \ V  V / 
 |_____/|_| |_|\__,_|_|\_\___|_| |_| |_|\__,_|_|\_\___| |_|  \_\_| \_/_/    \_\___/\___|\__, |     \/  \/ \___/|_|  |_|\_\_| |_|\___/ \_/\_/ 
                                                                                           | |                                                
                                                                                           |_|                                                
 Contact : alexandre.soriano@cirad.fr
    """
    )
    os.mkdir("./logs/run_"+current_time)
    f = open("./logs/run_"+current_time+"/pipeline_params_"+current_time+".txt", "a")
    f.write("\n\nAt : ")
    f.write(str(datetime.now().time()))
    f.write("\n\nConfiguration :\n\n")
    f.write(str(config))
    f.write("\n\nSample IDs : \n\n")
    # f.write(str(sample_ids))
    f.write(str(fastq_dict))
    with open("./logs/run_"+current_time+"/pipeline_params_"+current_time+".yaml", "w") as outfile:
        outfile.write("Configuration file : \n\n")
        yaml.dump(config, outfile, default_flow_style=False)
        outfile.write("\n\nSample list :\n\n")
        yaml.dump(fastq_dict, outfile, default_flow_style=False)
    f.close()
    f = open("./logs/last_launch.txt", "w")
    f.write(str(current_time))

# print(workflow.cores)


localrules: all, symlink_fastq, symlink_genome, stringtie_count_formating, featureCount_count_formating


wildcard_constraints:
    sample= "|".join(sample_keys),
    pseudo_mapping_tools="|".join(selected_pseudo_mapping_tools),
    mapping_tool="|".join(selected_mapping_tools),

rule all:
    input:
        # stringtieOutput=expand("03_quantification_results/{sample}/{sample}.gtf", sample=sample_keys),
        bigwigOutput=expand("04_results/bigwig_files/{mapping_tool}/{sample}.bw", sample=sample_keys, mapping_tool=selected_mapping_tools) if config["outputBigWig"]!=0 else [],
        flagstat=expand("logs/flagstat/{mapping_tool}/{sample}.txt", sample=sample_keys, mapping_tool=selected_mapping_tools) if config["checkBam"]!=0 else [],
        dea=expand("05_analysis/count_{mapping_tool}_{quantification_tool}/normalized_count.tsv", mapping_tool=selected_mapping_tools, quantification_tool = selected_quantification_tool) if (config["check_input_file_name"]==True) else [],
        dea_pseudoMap=expand("05_analysis/count_{pseudo_mapping_tools}/normalized_count.tsv", pseudo_mapping_tools=selected_pseudo_mapping_tools) if config["check_input_file_name"]==True else [],
        dea_rsem="05_analysis/count_rsem/normalized_count.tsv" if selected_rsem else [],
        fastqc=expand("raw_data/fastqc/{sample}/{sample}_R{n}_fastqc.html", sample=sample_keys, n=[1,2]),
        # marine_fastqc=expand("raw_data/fastqc/{sample}/{sample}_R{n}_fastqc.html", sample=sample_keys, n=[1,2]),
        # stringtie_gene_count=expand("04_results/count_{mapping_tool}/{mapping_tool}_stringtie_gene_count_matrix_formated.csv", mapping_tool=selected_mapping_tools),
        multiqc_raw="04_results/multiqc/raw_data/multiqc_report.html",
        multiqc_mapping=expand("04_results/multiqc/{mapping_tool}_processed_data/multiqc_report.html", mapping_tool=selected_mapping_tools),
        multiqc_pseudo_mapping=expand("04_results/multiqc/{pseudo_mapping_tools}_processed_data/multiqc_report.html",pseudo_mapping_tools=selected_pseudo_mapping_tools),
        multiqc_rsem="04_results/multiqc/rsem_processed_data/multiqc_report.html" if selected_rsem else [],
        # multiqc_star="04_results/multiqc/processed_data/multiqc_report.html",
        quantification_bam=expand("04_results/count_{mapping_tool}/{mapping_tool}_{quantification_tool}_gene_count_matrix_formated.csv", quantification_tool = selected_quantification_tool, mapping_tool=selected_mapping_tools) if(config["transcriptome"] == False) else [],
        quantification_pseudo_mapping=expand("04_results/count_{pseudo_mapping_tools}/{pseudo_mapping_tools}_counts.csv", pseudo_mapping_tools=selected_pseudo_mapping_tools),
        # featureCount_count_matrix_purif=expand("04_results/count_{mapping_tool}/{mapping_tool}_featureCount_count_matrix_purified.csv",mapping_tool=selected_mapping_tools),
        pseudo_mapping=expand("03_quantification_results/{pseudo_mapping_tools}/{sample}", sample=sample_keys, pseudo_mapping_tools=selected_pseudo_mapping_tools),
        rsem_mapping=expand("03_quantification_results/rsem/{sample}.genes.results", sample=sample_keys) if selected_rsem else [],
        rsem="04_results/count_rsem/rsem_counts_genelvl_formated.csv"  if selected_rsem else []


rule trimming_only:
    input:
        R1=expand("01_trimmed_reads/{sample}_R1.fastq.gz", sample=sample_keys),
        R2=expand("01_trimmed_reads/{sample}_R2.fastq.gz", sample=sample_keys)

rule mapping_only:
    input:
        bams=expand("02_mapped_reads/{mapping_tool}/final/{sample}.bam.bai", sample=sample_keys, mapping_tool=selected_mapping_tools) if config["mark_duplicates"]==True else expand("02_mapped_reads/{mapping_tool}/sorted/{sample}.bam.bai", sample=sample_keys, mapping_tool=selected_mapping_tools),
        pseudo_mapping=expand("03_quantification_results/{pseudo_mapping_tools}/{sample}", sample=sample_keys, pseudo_mapping_tools=selected_pseudo_mapping_tools)



rule prepare_index:
    input:
        hs2_genome=expand("raw_data/genome/{genome}.fasta", genome=genome_name),
        hs2_splice_sites="raw_data/genome/splice_sites.csv",
        hs2_exons="raw_data/genome/exons.csv",
        star_out_dir=expand("raw_data/genome/{genome}_star_index/", genome=genome_name),
        star_chr_name=expand("raw_data/genome/{genome}_star_index/chrName.txt", genome=genome_name),
        kallisto=expand("raw_data/genome/{genome}_tx.idx", genome=genome_name),
        salmon=expand("raw_data/genome/salmon_index", genome=genome_name)




# ipdb.set_trace()
#### General messages
# These messages will be print depending on the sucess or the failure of the pipeline
onsuccess:
    print(
"""
Workflow finished. Looks like everything was fine.
"""
    )
    copy_destination = config["output_main_results_dir"]
    if copy_destination != "":
        print("Most important dirs are being copied to " + copy_destination+"/rna_seq_pipeline_"+current_time)
        shutil.copytree("04_results", copy_destination+"/rna_seq_pipeline_"+current_time+"/04_results", symlinks = False)
        shutil.copytree("05_analysis", copy_destination+"/rna_seq_pipeline_"+current_time+"/05_analysis", symlinks = False)
        shutil.copytree("logs", copy_destination+"/rna_seq_pipeline_"+current_time+"/logs", symlinks = False)

onerror:
    print("An error occurred. You should read the readme file and see if everything have been setup the right way. If you encouter a persistent problem, feel free to contact alexandre.soriano@cirad.fr")

#### Workflow

rule symlink_fastq:
    input:
        read_1 = lambda wildcards: fastq_dict[wildcards.sample]["R1"],
        read_2 = lambda wildcards: fastq_dict[wildcards.sample]["R2"]
    output:
        R1="raw_data/fastq/{sample}_R1.fastq.gz",
        R2="raw_data/fastq/{sample}_R2.fastq.gz"
    resources:
        mem_mb=100,
        time=config["time_limit"]["instantaneous"],
        partition=config["queue"]["short"]
    shell:
        "ln -s {input.read_1} {output.R1} ; "
        "ln -s {input.read_2} {output.R2}"
    
rule symlink_genome:
    input:
        genome=expand("{genome}", genome=config["genome"])
    output:
        genome=expand("raw_data/genome/{genome}.fasta", genome=genome_name)
    resources:
        mem_mb=100,
        time=config["time_limit"]["instantaneous"],
        partition=config["queue"]["short"]
    shell:
        "ln -s {input.genome} {output.genome}"
        
#The genome also need to be indexed by samtools
rule samtools_faidx:
    input:
       genome=expand("raw_data/genome/{genome}.fasta", genome=genome_name)
    output:
       "raw_data/genome/{genome}.fasta.fai"
    envmodules:
        config["modules"]["samtools"]
    message:
        "Indexing {input}..."
    resources:
        mem_mb=1024,
        time=10,
        partition=config["queue"]["short"]
    conda:
        "envs/samtools.yaml"
    shell:
       "samtools faidx {input}"

rule fastqc:
    input:
        fastq="raw_data/fastq/{sample}_R{n}.fastq.gz"
    output:
        html="raw_data/fastqc/{sample}/{sample}_R{n}_fastqc.html",
        zipf="raw_data/fastqc/{sample}/{sample}_R{n}_fastqc.zip"
    threads:
        config["fastqc"]["threads"]
    resources:
        mem_mb=get_mem_fastqc,
        time = 600,
        partition=config["queue"]["normal"]
    envmodules:
        config["modules"]["fastqc"]
    params:
        outdir=directory("raw_data/fastqc/{sample}")
    benchmark:
        "logs/Benchmark/fastqc/{sample}_R{n}_bench.txt"
    conda:
        "envs/fastqc.yaml"
    shell:
        "mkdir -p {params.outdir} ; fastqc -t {threads} --outdir {params.outdir} {input}"

#Trimming with Cutadapt
rule cutadapt:
    input:
        read="raw_data/fastq/{sample}_R1.fastq.gz",
        read2="raw_data/fastq/{sample}_R2.fastq.gz"
    output:
        R1=temp("01_trimmed_reads/{sample}_R1.fastq.gz"),
        R2=temp("01_trimmed_reads/{sample}_R2.fastq.gz")
    threads: 
        config["Cutadapt"]["threads"]
    #message:
    #    "Cutadapt on {input}..."
    envmodules:
        config["modules"]["cutadapt"]
    priority:
        80
    benchmark:
        "logs/Benchmark/cutadapt/{sample}-bench.txt"
    resources:
        mem_mb=get_mem_cutadapt,
        time = 240,
        partition=config["queue"]["normal"]
    conda:
        "envs/cutadapt.yaml"
    singularity:
        config["singularity"]["cutadapt"]
    shell:
        "cutadapt -j {threads} -q {config[Cutadapt][Quality_value]} -m {config[Cutadapt][min_length]} -a {config[Cutadapt][forward_adapter]} -A {config[Cutadapt][reverse_adapter]} --max-n {config[Cutadapt][max_n]} -o {output.R1} -p {output.R2} {config[Cutadapt][options]} {input.read} {input.read2}"

rule hisat2_splice_sites:
    input:
       annotation=expand("{annotation}", annotation=config["annotation_gtf"])
    output:
       "raw_data/genome/splice_sites.csv"
    envmodules:
        config["modules"]["hisat2"],
        config["modules"]["python3"]
    resources:
        mem_mb=1024,
        time=config["time_limit"]["very_short"],
        partition=config["queue"]["short"]
    conda:
        "envs/hisat2.yaml"
    shell:
       "hisat2_extract_splice_sites.py {input.annotation} > {output}"

rule hisat2_exons:
    input:
       annotation=expand("{annotation}", annotation=config["annotation_gtf"])
    output:
       "raw_data/genome/exons.csv"
    envmodules:
        config["modules"]["hisat2"],
        config["modules"]["python3"]
    resources:
        mem_mb=1024,
        time=config["time_limit"]["very_short"],
        partition=config["queue"]["short"]
    conda:
        "envs/hisat2.yaml"
    shell:
       "hisat2_extract_exons.py {input} > {output}"

rule hisat2_build_index:
    input:
       genome=expand("raw_data/genome/{genome}.fasta", genome=genome_name),
       splice_sites="raw_data/genome/splice_sites.csv",
       exons="raw_data/genome/exons.csv"
    output:
        expand("raw_data/genome/{genome_prefix}.{n}.ht2",  genome_prefix=genome_name,  n=range(1,9),  allow_missing=False)
    envmodules:
        config["modules"]["hisat2"]
    conda:
        "envs/hisat2.yaml"
    priority:
        100
    params:
        path_prefix=expand("raw_data/genome/{genome}", genome=genome_name)
    resources:
        mem_mb=get_mem_hisat2_index,
        time=600,
        partition=config["queue"]["normal"]
    shell:
        "hisat2-build --ss {input.splice_sites} --exon {input.exons} {input.genome} {params.path_prefix}"

#Raw mapping, with conversion to Bam file with samtools. 
rule hisat2_mapping:
    input:
       genome=expand("raw_data/genome/{genome}.fasta", genome=genome_name),
       index=rules.hisat2_build_index.output,
       splice_sites="raw_data/genome/splice_sites.csv",
       exons="raw_data/genome/exons.csv",
       R1="01_trimmed_reads/{sample}_R1.fastq.gz",
       R2="01_trimmed_reads/{sample}_R2.fastq.gz"
    output:
        temp("02_mapped_reads/hisat2/sorted/{sample}.bam")
    threads:
        config["hisat2"]["threads"]
    params:
        path_prefix=expand("raw_data/genome/{genome}", genome=genome_name),
        rg_id="{sample}",
        rg_pl="PL:ILLUMINA",
        rg_sm="SM:{sample}"
    envmodules:
        config["modules"]["hisat2"],
        config["modules"]["samtools"]
    conda:
        "envs/hisat2.yaml"
    message:
        "Raw mapping with {input.R1} and {input.R2}..."
    benchmark:
        "logs/Benchmark/hisat2/{sample}-bench.txt"
    resources:
        mem_mb=get_mem_hisat_mapping,
        time=config["time_limit"]["normal"],
        partition=config["queue"]["normal"]
    shell:
        "hisat2 -p {threads} --rg-id {params.rg_id} --rg {params.rg_pl} --rg {params.rg_sm} -x {params.path_prefix} {config[hisat2][options]} --dta -1 {input.R1} -2 {input.R2}  | samtools sort -o {output} - "
#### STAR

rule star_build_index:
    input:
        genome=expand("raw_data/genome/{genome}.fasta", genome=genome_name),
        annotation=expand("{annotation}", annotation=config["annotation_gtf"])
    output:
        out_dir=directory("raw_data/genome/{genome}_star_index/"),
        chr_name="raw_data/genome/{genome}_star_index/chrName.txt"
    envmodules:
        config["modules"]["star"]
    conda:
        "envs/star.yaml"
    resources:
        mem_mb=24000,
        time=config["time_limit"]["normal"],
        partition=config["queue"]["normal"]
    params:
        max_read_length=read_length-1,
        genomeSAindexNbases=math.trunc(min(14, math.log(genome_size, 2)/2 - 1)) ##For small genomes, the parameter –genomeSAindexNbases must be scaled down tomin(14, log2(GenomeLength)/2 - 1)
    priority:
        100
    threads:
        4 
    shell:
        "STAR --runThreadN 4 --runMode genomeGenerate --genomeDir {output.out_dir} --genomeFastaFiles {input.genome} --sjdbGTFfile {input.annotation} --sjdbOverhang {params.max_read_length} --genomeSAindexNbases {params.genomeSAindexNbases}"

# ipdb.set_trace()


rule star_mapping:
    input:
        index_directory=expand("raw_data/genome/{genome}_star_index/", genome=genome_name),
        R1="01_trimmed_reads/{sample}_R1.fastq.gz",
        R2="01_trimmed_reads/{sample}_R2.fastq.gz"
    output:
        bam=temp("02_mapped_reads/star/sorted/{sample}.bam"),
        log="02_mapped_reads/star/sorted/{sample}_Log.final.out",
        pass1=temp(directory("02_mapped_reads/star/sorted/{sample}__STARpass1")),
        stargenome=temp(directory("02_mapped_reads/star/sorted/{sample}__STARgenome"))
    envmodules:
        config["modules"]["star"]
    conda:
        "envs/star.yaml"
    params:
        output_prefix="02_mapped_reads/star/sorted/{sample}_",
        rg="ID:{sample}\tPL:ILLUMINA\tSM:{sample}",
    threads:
        config["star"]["threads"]
    benchmark:
        "logs/Benchmark/star/{sample}-bench.txt"
    resources:
        mem_mb=get_mem_star_mapping,
        time=config["time_limit"]["normal"],
        partition=config["queue"]["normal"]
    shell:
        "STAR --runThreadN {threads} "
        "--genomeDir {input.index_directory} "
        "--readFilesIn {input.R1} {input.R2} "
        "--twopassMode Basic "
        "--outFileNamePrefix {params.output_prefix} "
        "--outSAMtype BAM SortedByCoordinate "
        "--outSAMunmapped Within "
        "--quantMode GeneCounts "
         "--outSAMattrRGline \"{params.rg}\" "
        "--outStd BAM_SortedByCoordinate " ###To name the output
        "--readFilesCommand zcat > {output.bam} "

# ipdb.set_trace()

#This is important for the variant calling step... It may or not be usefull in some other projects.
rule picard_markduplicates:
    input:
        bam="02_mapped_reads/{mapping_tool}/sorted/{sample}.bam"
    output:
        bam="02_mapped_reads/{mapping_tool}/final/{sample}.bam"
    log:
        log1="logs/picard_markduplicates/{mapping_tool}/{sample}.log"
    envmodules:
        config["modules"]["java"]
    conda:
        "envs/picard.yaml"
    params:
        temp_dir=my_temp_dir,
    resources:
        mem_mb=get_mem_picard_sample,
        time=config["time_limit"]["normal"],
        partition=config["queue"]["normal"]
    benchmark:
        "logs/Benchmark/picard/{mapping_tool}_{sample}-bench.txt"
    shell:
        "picard -Djava.io.tmpdir={params.temp_dir} -jar -Xms512m -Xmx{resources.mem_mb}m "
        "MarkDuplicates "
        "I={input.bam} "
        "O={output} "
        "VALIDATION_STRINGENCY=LENIENT "
        "MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 "
        "REMOVE_DUPLICATES=FALSE "
        "M={log.log1} "

#Index the bam file.
rule samtools_index:
    input:
        bam="02_mapped_reads/{mapping_tool}/final/{sample}.bam" if config["mark_duplicates"]==True else "02_mapped_reads/{mapping_tool}/sorted/{sample}.bam"
    output:
        bai="02_mapped_reads/{mapping_tool}/final/{sample}.bam.bai" if config["mark_duplicates"]==True else "02_mapped_reads/{mapping_tool}/sorted/{sample}.bam.bai"
    message:
        "Indexing {input}..."
    priority:
        10
    threads:
        config["samtools"]["threads"]
    envmodules:
        config["modules"]["samtools"]
    conda:
        "envs/samtools.yaml"
    resources:
        mem_mb=4096,
        time=config["time_limit"]["short"],
        partition=config["queue"]["short"]
    shell:
        "samtools index -@ {threads} {input.bam}"

#Compute some stats about bam files
rule samtools_flagstat:
    input:
        bam="02_mapped_reads/{mapping_tool}/final/{sample}.bam" if config["mark_duplicates"]==True else "02_mapped_reads/{mapping_tool}/sorted/{sample}.bam"
    output:
        stats="logs/flagstat/{mapping_tool}/{sample}.txt"
    message:
        "Computing some stats about {input}..."
    threads:
        config["samtools"]["threads"]
    envmodules:
        config["modules"]["samtools"]
    conda:
        "envs/samtools.yaml"
    resources:
        mem_mb=4096,
        time=config["time_limit"]["short"],
        partition=config["queue"]["short"]
    shell:
        "samtools flagstat -@ {threads} {input.bam} > {output.stats}"

#Creating bigWig files
rule deeptools_bamCoverage:
    input:
        bam="02_mapped_reads/{mapping_tool}/final/{sample}.bam" if config["mark_duplicates"]==True else "02_mapped_reads/{mapping_tool}/sorted/{sample}.bam",
        bai="02_mapped_reads/{mapping_tool}/final/{sample}.bam.bai" if config["mark_duplicates"]==True else "02_mapped_reads/{mapping_tool}/sorted/{sample}.bam.bai"
    output:
        bigwig="04_results/bigwig_files/{mapping_tool}/{sample}.bw"
    message:
        "Creating bigwig file for {input}..."
    threads:
        config["bamCoverage"]["threads"]
    envmodules:
        config["modules"]["deepTools"]
    conda:
        "envs/deeptools.yaml"
    resources:
        mem_mb=4096,
        time=2880,
        partition=config["queue"]["normal"]
    shell:
        "bamCoverage -p {threads} -bs 1 -b {input.bam} -o {output.bigwig}"

# Count part

## Get count from stringtie

rule stringTie_full:
    input:
        bam="02_mapped_reads/{mapping_tool}/final/{sample}.bam" if config["mark_duplicates"]==True else "02_mapped_reads/{mapping_tool}/sorted/{sample}.bam",
        bai="02_mapped_reads/{mapping_tool}/final/{sample}.bam.bai" if config["mark_duplicates"]==True else "02_mapped_reads/{mapping_tool}/sorted/{sample}.bam.bai",
        gff3=expand("{gff3}", gff3=config["annotation_gff3"])
    output:
        stringtie_gtf="03_quantification_results/{mapping_tool}/{sample}/{sample}.gtf"
    message:
        "Stringtie is running on {input.bam}"
    envmodules:
        config["modules"]["stringtie"]
    conda:
        "envs/stringtie.yaml"
    resources:
        mem_mb=4096,
        time=720,
        partition=config["queue"]["normal"]
    shell:
        "stringtie -e -B {config[stringtie][options]} -G {input.gff3} -o {output.stringtie_gtf} {input.bam}"

rule stringtie_get_count:
    input:
        stringtie_all_gtf=expand("03_quantification_results/{{mapping_tool}}/{sample}/{sample}.gtf", sample=sample_keys)
    output:
        gene_count="03_quantification_results/count_{mapping_tool}/{mapping_tool}_stringtie_gene_count_matrix.csv",
        transcript_count="03_quantification_results/count_{mapping_tool}/{mapping_tool}_stringtie_transcript_count_matrix.csv"
    params:
        readLength=read_length
    envmodules:
        config["modules"]["stringtie"]
    conda:
        "envs/stringtie.yaml"
    resources:
        mem_mb=4096,
        time=config["time_limit"]["very_short"],
        partition=config["queue"]["short"]
    shell:
        "prepDE.py -l {params.readLength} -g {output.gene_count} -t {output.transcript_count} -i 03_quantification_results/{wildcards.mapping_tool}"

rule stringtie_count_formating:
    input:
         gene_count="03_quantification_results/count_{mapping_tool}/{mapping_tool}_stringtie_gene_count_matrix.csv",
         transcript_count="03_quantification_results/count_{mapping_tool}/{mapping_tool}_stringtie_transcript_count_matrix.csv"
    output:
         gene_count="04_results/count_{mapping_tool}/{mapping_tool}_stringtie_gene_count_matrix_formated.csv",
         transcript_count="04_results/count_{mapping_tool}/{mapping_tool}_stringtie_transcript_count_matrix_formated.csv"
    resources:
        mem_mb=1024,
        time=config["time_limit"]["very_short"],
        partition=config["queue"]["short"]
    shell:
        "cat {input.gene_count} | (read -r; printf \"%s\\n\" \"$REPLY\"; sort) | sed 's/,/\\t/g' > {output.gene_count} ;" 
        "cat {input.transcript_count} | (read -r; printf \"%s\\n\" \"$REPLY\"; sort) | sed 's/,/\\t/g' > {output.transcript_count} " 

## Get count from featureCount

rule featureCount:
    input:
        bam=expand("02_mapped_reads/{{mapping_tool}}/final/{sample}.bam", sample=sample_keys) if config["mark_duplicates"]==True else expand("02_mapped_reads/{{mapping_tool}}/sorted/{sample}.bam", sample=sample_keys),
        bai=expand("02_mapped_reads/{{mapping_tool}}/final/{sample}.bam.bai", sample=sample_keys) if config["mark_duplicates"]==True else expand("02_mapped_reads/{{mapping_tool}}/sorted/{sample}.bam.bai", sample=sample_keys),
        gtf=expand("{gff3}", gff3=config["annotation_gtf"])
    output:
        featureCount_count_matrix="03_quantification_results/count_{mapping_tool}/{mapping_tool}_featureCount_count_matrix.csv",
    threads: config["featureCount"]["threads"]
    params:
        quality_cutoff=config["featureCount"]["quality_cutoff"],
        strand=config["featureCount"]["strand"],
        options=config["featureCount"]["options"]
    envmodules:
        config["modules"]["subread"]
    conda:
        "envs/subread.yaml"
    resources:
        mem_mb=get_mem_featureCount,
        time=10080,
        partition=config["queue"]["long"]
    shell:
        "featureCounts -T {threads} -Q {params.quality_cutoff} -s {params.strand} {params.options} -a {input.gtf} -o {output} {input.bam}"

rule featureCount_count_formating:
    input:
        featureCount_count_matrix="03_quantification_results/count_{mapping_tool}/{mapping_tool}_featureCount_count_matrix.csv"
    output:
        featureCount_count_matrix_purif="04_results/count_{mapping_tool}/{mapping_tool}_featureCount_gene_count_matrix_formated.csv"
    threads: 1
    resources:
        mem_mb=1024,
        time=config["time_limit"]["very_short"],
        partition=config["queue"]["short"]
    shell:
        "sed '1d' {input} | cut -f1,7- | sed '1s:\\t[^/]*/[^/]*/[^/]\+/\([[:print:]]\+\).bam:\\t\\1:g' | sed '1s/Geneid/Gene/' > {output}"


# Extract transcript sequences for rsem.

rule extract_transcript_sequences:
    input:
        genome=expand("raw_data/genome/{genome}.fasta", genome=genome_name),
        annotation=expand("{annotation}", annotation=config["annotation_gtf"]),
        genome_index=expand("raw_data/genome/{genome}.fasta.fai", genome=genome_name)
    output:
        transcriptome="raw_data/genome/{genome}_tx.fasta"
    resources:
        mem_mb=1024,
        time=config["time_limit"]["very_short"],
        partition=config["queue"]["short"]
    envmodules:
        config["modules"]["gffread"]
    conda:
        "envs/gffread.yaml"
    shell:
        "gffread -E -g {input.genome} -w  {output} {input.annotation}"


## Get count from rsem

##FIXME : this multiext will work only with bowtie2
rule rsem_prepare_refseq:
    input:
        transcriptome = choose_transcriptome_input()
    output:
        multiext("raw_data/rsem_index/{genome}",
            ".1.bt2",
            ".2.bt2",
            ".3.bt2",
            ".4.bt2",
            ".rev.1.bt2",
            ".rev.2.bt2")
    resources:
        mem_mb=8192,
        time=config["time_limit"]["normal"],
        partition=config["queue"]["normal"]
    envmodules:
        config["modules"]["rsem"]
    conda:
        "envs/rsem.yaml"
    params:
        mapping_tool="--bowtie2",
        output_prefix="raw_data/rsem_index/{genome}",
        tx_to_gene="--transcript-to-gene-map "+config["transcript_to_gene_mapping"] if config["transcript_to_gene_mapping"] else ""   
    shell:
        "rsem-prepare-reference {params.mapping_tool} {params.tx_to_gene} {input.transcriptome} {params.output_prefix}"

rule rsem_mapping:
    input: 
        index=expand("raw_data/rsem_index/{genome}.{idx}", genome=genome_name, idx=[ "1.bt2", "2.bt2","3.bt2","4.bt2","rev.1.bt2","rev.2.bt2"]),
        R1="01_trimmed_reads/{sample}_R1.fastq.gz",
        R2="01_trimmed_reads/{sample}_R2.fastq.gz"
    output:
        genes_res="03_quantification_results/rsem/{sample}.genes.results",
        isoform_res="03_quantification_results/rsem/{sample}.isoforms.results",
        bam="03_quantification_results/rsem/{sample}.transcript.bam"
    envmodules:
        config["modules"]["rsem"]
    conda:
        "envs/rsem.yaml"
    threads: config["rsem"]["threads"]
    params:
        mapping_tool="--bowtie2",
        index_location=expand("raw_data/rsem_index/{genome}", genome=genome_name),
        output_prefix="03_quantification_results/rsem/{sample}"
    shell:
        "rsem-calculate-expression --num-threads {threads} {params.mapping_tool} --paired-end {input.R1} {input.R2} {params.index_location} {params.output_prefix}"


rule rsem_quantification_gene_and_tx:
    input:
        gene_level=expand("03_quantification_results/rsem/{sample}.genes.results", sample=sample_keys),
        tx_level=expand("03_quantification_results/rsem/{sample}.isoforms.results", sample=sample_keys)
    output:
        counts_gene="03_quantification_results/rsem/rsem_counts_genelvl.csv",
        counts_tx="03_quantification_results/rsem/rsem_counts_txlvl.csv"
    envmodules:
        config["modules"]["rsem"]
    conda:
        "envs/rsem.yaml"
    shell:
        """
        rsem-generate-data-matrix {input.gene_level} > {output.counts_gene};
        rsem-generate-data-matrix {input.tx_level} > {output.counts_tx}
        """

rule rsem_count_formating:
    input:
        counts_gene="03_quantification_results/rsem/rsem_counts_genelvl.csv",
        counts_tx="03_quantification_results/rsem/rsem_counts_txlvl.csv"
    output:
        rsem_count_matrix_purif_gene="04_results/count_rsem/rsem_counts_genelvl_formated.csv",
        rsem_count_matrix_purif_tx="04_results/count_rsem/rsem_counts_txlv_formated.csv"
    threads: 1
    resources:
        mem_mb=1024,
        time=config["time_limit"]["very_short"],
        partition=config["queue"]["short"]
    shell:
        """
        sed "1s/.genes.results//g" {input.counts_gene} | sed '1s:03_quantification_results/rsem/::g' | sed 's/"//g' | sed "s/^/Gene/" > {output.rsem_count_matrix_purif_gene} ;
        sed "1s/.genes.results//g" {input.counts_tx} | sed '1s:03_quantification_results/rsem/::g' | sed 's/"//g' | sed "s/^/Gene/" > {output.rsem_count_matrix_purif_tx} 
        """

rule kallisto_index:
    input:
        transcriptome = choose_transcriptome_input()
        # if(config["transcriptome"] == True):
        #     transcriptome=config["transcriptome"]
        # else:
        #     transcriptome=expand("raw_data/genome/{genome}_tx.fasta", genome=genome_name)
    output:
        out=expand("raw_data/genome/{genome}_tx.idx", genome=genome_name) if config["transcriptome"]== "" else  expand("raw_data/genome/{genome}_tx.idx", genome=transcriptome_name)
    resources:
        mem_mb=8192,
        time=config["time_limit"]["short"],
        partition=config["queue"]["short"]
    envmodules:
        config["modules"]["kallisto"]
    conda:
        "envs/kallisto.yaml"
    shell:
        "kallisto index -i {output.out} {input.transcriptome}"

rule kallisto_mapping:
    input:
        index=rules.kallisto_index.output,
        R1="01_trimmed_reads/{sample}_R1.fastq.gz",
        R2="01_trimmed_reads/{sample}_R2.fastq.gz"
    output:
        abundance_h5="03_quantification_results/kallisto/{sample}/abundance.h5",
        abundance_tsv="03_quantification_results/kallisto/{sample}/abundance.tsv",
        output_dir=directory("03_quantification_results/kallisto/{sample}/"),
    resources:
        mem_mb=4096,
        time=config["time_limit"]["normal"],
        partition=config["queue"]["normal"]
    params:
        #output_dir="02_mapped_reads/{pseudo_mapping_tools}/{sample}/",
        options=config["kallisto"]["options"],
        strand=config["kallisto"]["strandness"]
    envmodules:
        config["modules"]["kallisto"]
    conda:
        "envs/kallisto.yaml"
    threads: config["kallisto"]["threads"]
    shell:
        "kallisto quant -i {input.index} "
        "-o {output.output_dir} "
        "--threads={threads} "
        "{params.options} {params.strand} "
        "{input.R1} {input.R2} "        

rule salmon_decoy_preparation:
    input:
        transcriptome = choose_transcriptome_input(),
        genome=expand("raw_data/genome/{genome}.fasta", genome=genome_name)
    output:
        out_decoy="raw_data/genome/salmon_decoy.txt",
        out_gentrome="raw_data/genome/salmon_gentrome.fasta.gz"
    resources:
        mem_mb=4096,
        time=config["time_limit"]["short"],
        partition=config["queue"]["short"]
    params:
        gentrome="raw_data/genome/salmon_gentrome.fasta"
    shell:
        "grep \"^>\" {input.genome} | cut -f1 -d\" \" | sed \"s/>//\" > {output.out_decoy} ; "
        "cat {input.transcriptome} {input.genome} > {params.gentrome} ; "
        "gzip {params.gentrome}"


rule salmon_index:
    input:
        gentrome="raw_data/genome/salmon_gentrome.fasta.gz" if config["genome"]!="" else config["transcriptome"],
        decoy="raw_data/genome/salmon_decoy.txt"  if config["genome"]!="" else []
    output:
        out=directory("raw_data/genome/salmon_index")
    resources:
        mem_mb=16384,
        time=config["time_limit"]["normal"],
        partition=config["queue"]["normal"]
    envmodules:
        config["modules"]["salmon"]
    conda:
        "envs/salmon.yaml"
    threads: config["salmon"]["threads"]
    params:
        decoy="-d raw_data/genome/salmon_decoy.txt"  if config["genome"]!="" else []
    shell:
        "salmon index -p {threads} --transcripts {input.gentrome} {params.decoy} --index {output} "


rule salmon_quantification:
    input:
        index=rules.salmon_index.output,
        R1="01_trimmed_reads/{sample}_R1.fastq.gz",
        R2="01_trimmed_reads/{sample}_R2.fastq.gz"
    output:
        output_dir=directory("03_quantification_results/salmon/{sample}")
    resources:
        mem_mb=8192,
        time=config["time_limit"]["normal"],
        partition=config["queue"]["normal"]
    params:
        output_dir="03_quantification_results/salmon/{sample}/",
        options=config["salmon"]["options"]
    envmodules:
        config["modules"]["salmon"]
    conda:
        "envs/salmon.yaml"
    threads: config["salmon"]["threads"]
    shell:
        "salmon quant "
        "-i {input.index} "
        "{params.options} "
        "-l A "
        "-1 {input.R1} "
        "-2 {input.R2} "
        "-p {threads} "
        "-o {output.output_dir} "

# quantification_pseudo_mapping=expand("04_results/count_{pseudo_mapping_tools}/{pseudo_mapping_tools}_counts.csv", pseudo_mapping_tools=selected_pseudo_mapping_tools),
rule R_extract_pseudomapping_counts:
    input:
        # pseudo_mapping_res=directory("03_quantification_results/{pseudo_mapping_tools}/")
        pseudo_mapping_res=expand("03_quantification_results/{{pseudo_mapping_tools}}/{sample}", sample=sample_keys)
    output:
        counts="04_results/count_{pseudo_mapping_tools}/{pseudo_mapping_tools}_counts.csv"
    envmodules:
        config["modules"]["R"]
    conda:
        "envs/R.yaml"
    resources:
        mem_mb=2048,
        time=config["time_limit"]["short"],
        partition=config["queue"]["short"]
    params:
        input_dir="03_quantification_results/{pseudo_mapping_tools}/",
        tx_to_gene="--mapping_tx " + config["transcript_to_gene_mapping"]  if config["transcript_to_gene_mapping"]!="" else []
    shell:
        "Rscript ./R/extract_count_from_pseudomapping.R --input {params.input_dir}  --tool {wildcards.pseudo_mapping_tools} --output {output} {params.tx_to_gene}"

## Quality control part

rule R_analysis:
    input: 
        count_matrix="04_results/count_{mapping_tool}/{mapping_tool}_{quantification_tool}_gene_count_matrix_formated.csv"
    output:
        dea="05_analysis/count_{mapping_tool}_{quantification_tool}/normalized_count.tsv"
    envmodules:
        config["modules"]["R"]
    conda:
        "envs/R.yaml"
    resources:
        mem_mb=4096,
        time=config["time_limit"]["short"],
        partition=config["queue"]["short"]
    params:
        output_dir="05_analysis/count_{mapping_tool}_{quantification_tool}/"
    shell:
        "Rscript ./R/perform_norm_deg_pca.R --file {input} --output {params.output_dir}"

rule R_analysis_pseudomap:
    input: 
        pseudo_mapping_res=expand("03_quantification_results/{{pseudo_mapping_tools}}/{sample}", sample=sample_keys)
    output:
        dea="05_analysis/count_{pseudo_mapping_tools}/normalized_count.tsv"
    envmodules:
        config["modules"]["R"]
    conda:
        "envs/R.yaml"
    resources:
        mem_mb=4096,
        time=config["time_limit"]["short"],
        partition=config["queue"]["short"]
    params:
        input_dir="03_quantification_results/{pseudo_mapping_tools}/",
        output_dir="05_analysis/count_{pseudo_mapping_tools}/"
    shell:
        "Rscript ./R/perform_norm_deg_pca.R --directory {params.input_dir} --output {params.output_dir}"

use rule R_analysis as R_analysis_rsem with:
    input:
        count_matrix="04_results/count_rsem/rsem_counts_genelvl_formated.csv"
    output:
        dea="05_analysis/count_rsem/normalized_count.tsv"
    params:
        output_dir="05_analysis/count_rsem/"

rule multiqc_raw_data:
    input:
        R1=expand("01_trimmed_reads/{sample}_R1.fastq.gz", sample=sample_keys),
        R2=expand("01_trimmed_reads/{sample}_R2.fastq.gz", sample=sample_keys),
        fasqtc_html=expand("raw_data/fastqc/{sample}/{sample}_R{n}_fastqc.html", sample=sample_keys, n=[1,2]),
        fastqc_zip=expand("raw_data/fastqc/{sample}/{sample}_R{n}_fastqc.zip", sample=sample_keys, n=[1,2])
    output:
        outdir="04_results/multiqc/raw_data/multiqc_report.html"
    envmodules:
        config["modules"]["multiqc"]
    conda:
        "envs/multiqc.yaml"
    resources:
        mem_mb=4096,
        time=config["time_limit"]["normal"],
        partition=config["queue"]["normal"]
    params:
        output_dir="04_results/multiqc/raw_data/"
    shell:
        "multiqc . --outdir {params.output_dir} "
        "--module fastqc "
        "--module cutadapt "

rule multiqc_mapping:
    input:
        count_matrix="04_results/count_{mapping_tool}/{mapping_tool}_featureCount_gene_count_matrix_formated.csv", ##Stringtie is not suported.
        final_bam=expand("02_mapped_reads/{{mapping_tool}}/final/{sample}.bam", sample = sample_keys) if config["mark_duplicates"]==True else expand("02_mapped_reads/{{mapping_tool}}/sorted/{sample}.bam", sample=sample_keys)
    output:
        out_file="04_results/multiqc/{mapping_tool}_processed_data/multiqc_report.html",
        input_file_list=temp("file_list_{mapping_tool}.txt")
    envmodules:
        config["modules"]["multiqc"]
    conda:
        "envs/multiqc.yaml"
    resources:
        mem_mb=4096,
        time=600,
        partition=config["queue"]["normal"]
    params:
        output_dir="04_results/multiqc/{mapping_tool}_processed_data/"
    shell:
        "find -name \"*{wildcards.mapping_tool}*\" | grep -v \"00_diagnostic\" > file_list_{wildcards.mapping_tool}.txt ;"
        "multiqc . --outdir {params.output_dir} "
        "--module {wildcards.mapping_tool} "
        "--module picard "
        "--module samtools "
        "--module featureCounts "

rule multiqc_pseudo_mapping:
    input:
        pseudo_mapping=expand("03_quantification_results/{{pseudo_mapping_tools}}/{sample}", sample=sample_keys), 
    output:
        out_file="04_results/multiqc/{pseudo_mapping_tools}_processed_data/multiqc_report.html",
        input_file_list=temp("file_list_{pseudo_mapping_tools}.txt")
    envmodules:
        config["modules"]["multiqc"]
    conda:
        "envs/multiqc.yaml"
    resources:
        mem_mb=4096,
        time=600,
        partition=config["queue"]["normal"]
    params:
        output_dir="04_results/multiqc/{pseudo_mapping_tools}_processed_data/"
    shell:
        "find -name \"*{wildcards.pseudo_mapping_tools}*\"  | grep -v \"00_diagnostic\"  > file_list_{wildcards.pseudo_mapping_tools}.txt ;"
        "multiqc . --outdir {params.output_dir} "
        "--module {wildcards.pseudo_mapping_tools} "

rule multiqc_rsem:
    input:
        count_matrix="04_results/count_rsem/rsem_counts_genelvl_formated.csv"
    output:
        out_file="04_results/multiqc/rsem_processed_data/multiqc_report.html",
    envmodules:
        config["modules"]["multiqc"]
    conda:
        "envs/multiqc.yaml"
    resources:
        mem_mb=4096,
        time=600,
        partition=config["queue"]["normal"]        
    params:
        output_dir="04_results/multiqc/rsem_processed_data/"
    shell:
        "find -name \"*rsem*\"  | grep -v \"00_diagnostic\"  > file_list_rsem.txt ;"
        "multiqc . --outdir {params.output_dir} "
        "--module rsem --module bowtie2"

    

##############################
### Strandness inference stuff : WORK IN PROGRESS

rule quality_controll_and_diagnostic:
    input:
        bed=expand("raw_data/genome/{genome}.bed", genome = genome_name) if config["genome"]!="" else [],
        outfile=expand("00_diagnostic/strandness_inference/{mapping_tool}/{sample}.txt", sample=sample_keys, mapping_tool=selected_mapping_tools) if config["genome"]!="" else [],
        R1=expand("00_diagnostic/fastq/{sample}_R1.fastq.gz", sample=sample_keys) if config["genome"]!="" else [],
        R2=expand("00_diagnostic/fastq/{sample}_R2.fastq.gz", sample=sample_keys) if config["genome"]!="" else [],
        fastqc=expand("raw_data/fastqc/{sample}/{sample}_R{n}_fastqc.html", sample=sample_keys, n=[1,2]),
        distribution_intro="00_diagnostic/intron_size/quantile_distribution.txt" if config["genome"]!="" else []

rule subset_fastq:
    input:
        read_R1="raw_data/fastq/{sample}_R1.fastq.gz",
        read_R2="raw_data/fastq/{sample}_R2.fastq.gz"
    output:
        R1="00_diagnostic/fastq/{sample}_R1.fastq.gz",
        R2="00_diagnostic/fastq/{sample}_R2.fastq.gz"
    resources:
        mem_mb=1024,
        time=config["time_limit"]["very_short"],
        partition=config["queue"]["short"]
    params:
        R1_uncompress="00_diagnostic/fastq/{sample}_R1.fastq",
        R2_uncompress="00_diagnostic/fastq/{sample}_R2.fastq"
    run: ##Bash zcat | head technique does not work... So here is a weird python solution, with code duplication. 
        import sys, gzip
        gzf = gzip.GzipFile(input.read_R1, 'rb')
        outFile_R1 = gzip.GzipFile(output.R1,"wb", compresslevel=1)
        numLines = 0
        maxLines = 4000000
        for line in gzf:
            if numLines >= maxLines:
                break
            outFile_R1.write(line)
            numLines += 1
        outFile_R1.close()
        gzf.close()
        numLines = 0
        gzf_2 = gzip.GzipFile(input.read_R2, 'rb')
        outFile_R2 = gzip.GzipFile(output.R2,"wb", compresslevel=1)
        for line in gzf_2:
            if numLines >= maxLines:
                break
            outFile_R2.write(line)
            numLines += 1
        outFile_R2.close()
        gzf_2.close()
    
use rule hisat2_mapping as fast_hisat2_map with:
    input:
        genome=expand("raw_data/genome/{genome}.fasta", genome=genome_name),
        index=rules.hisat2_build_index.output,
        splice_sites="raw_data/genome/splice_sites.csv",
        exons="raw_data/genome/exons.csv",
        R1="00_diagnostic/fastq/{sample}_R1.fastq.gz",
        R2="00_diagnostic/fastq/{sample}_R2.fastq.gz"
    output:
        temp("00_diagnostic/hisat2/{sample}.bam")

use rule star_mapping as fast_star_map with:
    input:
        R1="00_diagnostic/fastq/{sample}_R1.fastq.gz",
        R2="00_diagnostic/fastq/{sample}_R2.fastq.gz",
        index_directory=expand("raw_data/genome/{genome}_star_index/", genome=genome_name)
    output:
        bam=temp("00_diagnostic/star/{sample}.bam"),
        log="00_diagnostic/star/{sample}_Log.final.out",
        pass1=temp(directory("00_diagnostic/star/{sample}__STARpass1")),
        stargenome=temp(directory("00_diagnostic/star/{sample}__STARgenome"))
    params:
        output_prefix="00_diagnostic/star/{sample}_",
        rg="ID:{sample}\tPL:ILLUMINA\tSM:{sample}",

#Convert gtf input to bed.
rule convert2bed_gtf_to_bed:
    input:
        annotation=expand("{annotation}", annotation=config["annotation_gtf"])
    output:
        bed="raw_data/genome/{genome}.bed"
    envmodules:
        config["modules"]["bedops"]
    resources:
        mem_mb=1024,
        time=10,
        partition=config["queue"]["short"]
    conda:
        "envs/bedops.yaml"
    shell:
        "convert2bed --input=gtf --output=BED < {input.annotation} > {output.bed} "


###Find strandness based on bam files.
rule rseqc_strandness_inference:
    input:
        bam="00_diagnostic/{mapping_tool}/{sample}.bam",
        bed=expand("raw_data/genome/{genome}.bed", genome = genome_name)
    output:
        outfile="00_diagnostic/strandness_inference/{mapping_tool}/{sample}.txt"
    envmodules:
        config["modules"]["rseqc"]
    conda:
        "envs/rseqc.yaml"
    resources:
        mem_mb=2048,
        time=config["time_limit"]["very_short"],
        partition=config["queue"]["short"]
    shell:
        "infer_experiment.py -i {input.bam} -r {input.bed} > {output.outfile} "

rule R_get_intron_length:
    input:
        annotation=expand("{annotation}", annotation=config["annotation_gff3"])
    output:
        "00_diagnostic/intron_size/quantile_distribution.txt"
    conda:
        "envs/R.yaml"
    envmodules:
        config["modules"]["R"]
    resources:
        mem_mb=4096,
        time=20,
        partition=config["queue"]["short"]
    params:
        outdir="00_diagnostic/intron_size/"
    shell:
        "Rscript R/quantile_from_gff.R --gff {input.annotation} --output {params.outdir} "
